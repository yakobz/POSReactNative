import LocalizedString from 'react-native-localization'

let Strings = new LocalizedString({
    en:{
        main_screen: 'Main screen',
        order_archive: 'Order archive',
        cash_in_out: 'Cash In/Out',
        devices_settigs: 'Devices settings',
        worklog_staff: 'Worklog staff',
        reports: 'Reports',
        checked_in_clients: 'Checked in clients',
        info: 'Info'
    },

    ru:{
        main_screen: 'Главный экран',
        order_archive: 'Архив заказов',
        cash_in_out: 'Кэш',
        devices_settigs: 'Настройки устройства',
        worklog_staff: 'Трекинг персонала',
        reports: 'Отчеты',
        checked_in_clients: 'Зачекиненные клиенты',
        info: 'Информация'
    },
})

export default Strings