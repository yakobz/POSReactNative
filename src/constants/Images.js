export const arrow = require('../../resources/images/arrow/arrow.png')
export const arrowdown_active = require('../../resources/images/arrow/arrowdown_active.png')
export const arrowdown_notactive = require('../../resources/images/arrow/arrowdown_notactive.png')
export const arrowup_active = require('../../resources/images/arrow/arrowup_active.png')
export const arrowup_notactive = require('../../resources/images/arrow/arrowup_notactive.png')

export const booking_soon = require('../../resources/images/booking/booking_soon.png')
export const booking = require('../../resources/images/booking/booking.png')

export const clocks = require('../../resources/images/clocks/clocks.png')

export const delivery = require('../../resources/images/delivery/delivery.png')
export const deliveryactive = require('../../resources/images/delivery/deliveryactive.png')

export const error = require('../../resources/images/error/error.png')

export const guests_busy = require('../../resources/images/guests/guests_busy.png')
export const guests_free = require('../../resources/images/guests/guests_free.png')

export const login = require('../../resources/images/login/login.png')

export const orderbar = require('../../resources/images/order/orderbar.png')
export const ordericon = require('../../resources/images/order/ordericon.png')
export const ordernotactive = require('../../resources/images/order/ordernotactive.png')

export const greenplus = require('../../resources/images/plus/greenplus.png')
export const plusBlue = require('../../resources/images/plus/plusblue.png')
export const plusOrange = require('../../resources/images/plus/plusorange.png')

export const cash = require('../../resources/images/sidebar/cash/cash.png')
export const cashactive = require('../../resources/images/sidebar/cash/cashactive.png')
export const checkin = require('../../resources/images/sidebar/checkin/checkin.png')
export const checkinactive = require('../../resources/images/sidebar/checkin/checkinactive.png')
export const device = require('../../resources/images/sidebar/device/device.png')
export const deviceactive = require('../../resources/images/sidebar/device/deviceactive.png')
export const home = require('../../resources/images/sidebar/home/home.png')
export const homeactive = require('../../resources/images/sidebar/home/homeactive.png')
export const info = require('../../resources/images/sidebar/info/info.png')
export const infoactive = require('../../resources/images/sidebar/info/infoactive.png')
export const archive = require('../../resources/images/sidebar/orderarchive/archive.png')
export const archiveactive = require('../../resources/images/sidebar/orderarchive/archiveactive.png')
export const reports = require('../../resources/images/sidebar/reports/reports.png')
export const reportsactive = require('../../resources/images/sidebar/reports/reportsactive.png')
export const staff = require('../../resources/images/sidebar/staff/staff.png')
export const staffactive = require('../../resources/images/sidebar/staff/staffactive.png')

export const menu_icon = require('../../resources/images/menu_icon.png')
export const pin_code_background = require('../../resources/images/pin_code_background.png')
export const shortline = require('../../resources/images/shortline.png')
