// BLUE
export const CATALINA_BLUE = '#2b3f5b'
export const CURIOUS_BLUE = '#2e7ec5'
export const POLO_BLUE = '#8db2da'
export const ALICE_BLUE = '#f1f2f3'
export const PATTENS_BLUE = '#c0dffb'
export const PATTENS_BLUE_WAITING_AREA = '#d7ebfd'
export const CHAMBRAY = '#516178'

// RED
export const TOMATO = '#ff5752'
export const NEON_CARROT = '#ff9b37'
export const TANGERINE = '#f48111'
export const PEACH_ORANGE = '#ffcd9b'
export const TANGERINE_TEXT = '#f17b09'
export const SUNSHADE = '#f79f4a'

// GRAY
export const ONAHAU = '#cee0e4'
export const JUNGLE_MIST = '#b7c3c3'
export const WHISPER = '#ececec'
export const WHITE_SMOKE = '#f3f3f3'
export const WHITE_SMOKE_LINE = '#f5f5f5'
export const WHITE_SMOKE_FILL = '#f2f2f2'
export const SWAMP = '#2b3232'
export const SILVER = '#bdbdbd'
export const STORM_GREY = '#79797d'
export const SNOW = '#fbfbfb'

// GREEN
export const CARIBBEAN_GREEN = '#0ecab2'
export const CARIBBEAN_GREEN_BUTTON = '#09c0a9'
export const PERSIAN_GREEN = '#06a792'
export const ICE_COLD = '#9feae0'
export const PERSIAN_GREEN_TEXT = '#04a28e'

// BROWN
export const DARK_BROWN = '#624225'