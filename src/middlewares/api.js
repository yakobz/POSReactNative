import * as types from '../constants/ActionTypes'

const middleware = store => next => action => {
    if (action.type !== types.API) {
        return next(action)
    }

    store.dispatch({
        type: action.action,
        payload: {...action.payload, loading: true}
    })

    setTimeout(() => {
        store.dispatch({
            type: action.action,
            payload: {...action.promise, loading: false}
        })
    }, 1000)
}

export default middleware