import React from 'react'
import { Provider } from 'react-redux'
import store from './store'
import LoginScreen from './components/screens/LoginScreen/LoginScreenContainer'
import PinCodeScreen from './components/screens/PinCodeScreen/PinCodeScreenContainer'
import MainScreen from './components/screens/MainScreen/MainScreenContainer'
import {Router, Scene, Stack} from 'react-native-router-flux'
import * as screens from './constants/Screens'
import { 
    Image,
    TouchableOpacity
} from 'react-native'

class RootScreen extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <Router>
                    <Stack >
                        <Scene key = {screens.LOGIN_SCREEN} component = {LoginScreen} hideNavBar/>
                        <Scene key = {screens.PINCODE_SCREEN} component = {PinCodeScreen} hideNavBar/>
                        <Scene key = {screens.MAIN_SCREEN} component = {MainScreen} hideNavBar/>
                    </Stack>
                </Router>
            </Provider>
        )
    }
}

export default RootScreen