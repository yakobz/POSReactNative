import React from 'react'
import ZonesView from '../../organisms/tables/ZonesView'
import TablesView from '../../organisms/tables/TablesView'
import BarListView from '../../molecules/BarListView'
import BarAndWaitingAreaView from '../../organisms/tables/BarAndWaitingAreaView'
import { 
    View
} from 'react-native'

barsCount = 50

class Tables extends React.Component {
    tablesContent() {
        return <View style = {{flexDirection: 'row', flex: 14}}>
            <BarAndWaitingAreaView style = {{flex: 0.27}}/>
            <TablesView style = {{flex: 2}} />
        </View>
    }

    barListView() {
        if (barsCount > 0) {
            return <BarListView
                style = {{flex: 0.9}}
                barsCount = {barsCount}
            />
        }

        return null
    }

    render() {
        return (
            <View style = {[this.props.style, {flex: 1}]}>
                <ZonesView style = {{flex: 1}} />
                {this.barListView()}
                {this.tablesContent()}
            </View>
        )
    }
}

export default Tables