import React from 'react'
import {px2dp} from '../../utils/utils'
import * as colors from '../../constants/Colors'
import * as images from '../../constants/Images'
import { 
    View,
    Text,
    StyleSheet,
    Image
} from 'react-native'

class ErrorTextWithImage extends React.Component {
    errorImage() {
        return <Image
            source = {images.error}
        />
    }

    errorText() {
        return <Text style = {styles.errorText}>
            {this.props.errorText}
        </Text>
    }

    render() {
        return(
            <View style = {this.props.style}>
                {this.errorImage()}
                {this.errorText()}
            </View>
        )
    }
}

const styles = StyleSheet.create ({
    errorText: {
        color: colors.TOMATO,
        fontSize: px2dp(32),
        paddingLeft: px2dp(15)
    }
})

export default ErrorTextWithImage