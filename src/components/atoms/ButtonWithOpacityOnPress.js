import React from 'react'
import { 
    View,
    TouchableWithoutFeedback,
    Text,
    StyleSheet
} from 'react-native'

class ButtonWithOpacityOnPress extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            opacity: 0
        }
    }

    blueButton() {
        return <TouchableWithoutFeedback
            onPress = {this.props.onPress}
            onPressIn = {() => {
                this.setState({
                    opacity: 0.2
                })
            }}
            onPressOut = {() => {
                this.setState({
                    opacity: 0
                })
            }}
        >
            <View style = {[styles.blueButton, {backgroundColor: this.props.backgroundColor}]}>
                <View style = {[styles.shadowView, {
                    backgroundColor: `rgba(0, 0, 0, ${this.state.opacity})`,
                    borderRadius: 5,
                }]}>
                    <Text style = {[styles.buttonTitle, {
                        opacity: 1 - this.state.opacity, 
                        color: this.props.textColor,
                        fontSize: this.props.fontSize}]}>
                        {this.props.title}
                    </Text>
                </View>
            </View>
        </TouchableWithoutFeedback>
    }

    render() {
        return(
            <View style = {this.props.style}>
                {this.blueButton()}
            </View>
        )
    }
}

const styles = StyleSheet.create ({
    blueButton: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        borderRadius: 5
    },

    buttonTitle: {
        alignSelf: "center"
    },

    shadowView: {
        alignSelf: "stretch", 
        flex: 1, 
        justifyContent: 'center'
    }
})

export default ButtonWithOpacityOnPress