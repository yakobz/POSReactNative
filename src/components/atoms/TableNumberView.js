import React from 'react'
import {px2dp} from '../../utils/utils'
import * as colors from '../../constants/Colors'
import {
    View,
    Text,
    StyleSheet
} from 'react-native'

class TableNumberView extends React.Component {
    backgroundColor() {
        return {backgroundColor: this.props.tableState === 'busy' ? colors.TANGERINE : colors.PERSIAN_GREEN}
    }


    render() {
        return (
            <View style = {[styles.tableNumberView, this.backgroundColor(), this.props.style]}>
                <Text style = {styles.title}>
                    {this.props.title}
                </Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    tableNumberView: {
        minWidth: px2dp(106),
        borderRadius: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },

    title: {
        marginLeft: px2dp(20),
        marginRight: px2dp(20),
        marginTop: px2dp(10),
        marginBottom: px2dp(10),
        color: 'white',
        fontSize: px2dp(32),
        fontWeight: 'bold'
    }
})

export default TableNumberView