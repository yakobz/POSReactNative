import React from 'react'
import {px2dp} from '../../utils/utils'
import * as colors from '../../constants/Colors'
import * as images from '../../constants/Images'
import {
    View,
    Image,
    Text,
    TouchableWithoutFeedback,
    StyleSheet
} from 'react-native'

class BarQuickOrdersView extends React.Component {
    barQuickOrdersView() {
        return <TouchableWithoutFeedback
            onPress = {this.props.onPress}
        >
            <View style = {styles.barQuickOrdersView}>
                <Image
                    source = {images.orderbar}
                />
                <Text style = {styles.title}>
                    {this.props.ordersCount}
                </Text>
            </View> 
        </TouchableWithoutFeedback>
    }

    render() {
        return this.barQuickOrdersView()
    }
}

const styles = StyleSheet.create({
    barQuickOrdersView: {
        justifyContent: 'space-around',
        alignItems: 'center',
        backgroundColor: 'rgba(255, 255, 255, 0.24)',
        width: px2dp(98),
        height: px2dp(160),
        marginTop: px2dp(20),
        borderRadius: 8
    },

    title: {
        fontSize: px2dp(34),
        color: colors.DARK_BROWN
    }
})

export default BarQuickOrdersView