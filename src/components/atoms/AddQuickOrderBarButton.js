import React from 'react'
import TiledImage from './TiledImage'
import * as images from '../../constants/Images'
import * as colors from '../../constants/Colors'
import {px2dp} from '../../utils/utils'
import {
    View,
    TouchableWithoutFeedback,
    Image,
    ImageBackground,
    Dimensions,
    StyleSheet,
    Platform
} from 'react-native'

class AddQuickOrderBarButton extends React.Component {
    tiledImage() {
        if (Platform.OS === 'ios') {
            return <ImageBackground
                style = {styles.backgroundImage}
                source = {images.shortline}
                resizeMode = 'repeat'
            />
        }

        return <TiledImage width = {this.props.width} height = {this.props.height}/>
    }

    addQuickOrderButtonStyle() {
        return {
            width: this.props.width,
            marginTop: this.props.offset,
            marginBottom: this.props.offset
        }
    }

    addQuickOrderBarButton() {
        return <TouchableWithoutFeedback
            onPress = {this.props.onPress}
        >
            <View style = {[styles.addQuickOrderBarButton, this.addQuickOrderButtonStyle(), this.props.style]}>
                {this.tiledImage()}
                <Image
                    source = {this.props.image}
                />
            </View>
        </TouchableWithoutFeedback>        
    }

    render() {
        return this.addQuickOrderBarButton()
    }
}

const styles = StyleSheet.create({
    addQuickOrderBarButton: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 8,
        overflow: 'hidden'
    },

    backgroundImage: {
        width: '100%',
        height: '100%',
        position: 'absolute'
    }
})

export default AddQuickOrderBarButton