import React from 'react'
import * as colors from '../../constants/Colors'
import {px2dp} from '../../utils/utils'
import { 
    View,
    StyleSheet
} from 'react-native'

size = 36
fillColor = colors.CURIOUS_BLUE

class PinCodeSymbol extends React.Component {
    backgroundColor() {
        return this.props.filled ? fillColor : 'transparent'
    }

    render() {
        return(
            <View style = {[styles.pinCodeSymbol, {backgroundColor: this.backgroundColor()}]}>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    pinCodeSymbol: {
        width: px2dp(size),
        height: px2dp(size),
        borderRadius: px2dp(0.5 * size),
        borderColor: fillColor,
        borderWidth: px2dp(4)
    }
})

export default PinCodeSymbol