import React from 'react'
import {px2dp} from '../../utils/utils'
import * as colors from '../../constants/Colors'
import * as images from '../../constants/Images'
import {
    View,
    Image,
    Text,
    StyleSheet
} from 'react-native'

class TableNumberOfGuestsView extends React.Component {
    backgroundColor() {
        return {backgroundColor: this.props.tableState === 'busy' ? colors.PEACH_ORANGE : colors.ICE_COLD}
    }

    textColor() {
        return {color: this.props.tableState === 'busy' ? colors.TANGERINE_TEXT : colors.PERSIAN_GREEN_TEXT}
    }

    numberOfGuestsImage() {
        return <Image
            source = {this.props.tableState === 'busy' ? images.guests_busy : images.guests_free}
        />
    }

    numberOfGuestsNumber() {
        return <Text style = {[styles.text, this.textColor()]}>
            {this.props.numberOfGuests}
        </Text>
    }

    numberOfGuestsView() {
        return <View style = {[styles.numberOfGuests, this.backgroundColor(), this.props.style]} >
            {this.numberOfGuestsImage()}
            {this.numberOfGuestsNumber()}
        </View>
    }

    render() {
        return this.numberOfGuestsView()
    }
}

const styles = StyleSheet.create({
    numberOfGuests: {
        width: px2dp(196),
        height: px2dp(56),
        borderRadius: 50,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center'
    },

    text: {
        left: px2dp(-35),
        fontSize: px2dp(32),
        fontWeight: '500'
    }
})

export default TableNumberOfGuestsView