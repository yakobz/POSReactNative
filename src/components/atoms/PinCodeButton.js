import React from 'react'
import { 
    TouchableWithoutFeedback,
    Text,
    View,
    Image,
    StyleSheet
} from 'react-native'

class PinCodeButton extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            opacity: 0
        }
    }

    title() {
        if (this.props.title) {
            return <Text style={{
                fontSize: this.props.fontSize, 
                color: this.props.textColor ? this.props.textColor : 'black',
                fontWeight : this.props.fontWeight ? this.props.fontWeight : '400'
            }}>
                {this.props.title}
            </Text>
        }

        return null
    }

    image() {
        if (this.props.image) {
            return <Image
                source = {this.props.image}
            />
        }

        return null
    }

    render() {
        return(
            <TouchableWithoutFeedback
                onPress = {this.props.onPress}
                onPressIn = {() => {
                    this.setState({
                        opacity: 0.05
                    })
                }}
                onPressOut = {() => {
                    this.setState({
                        opacity: 0
                    })
                }}
            >
                <View style={[this.props.style, styles.buttonView, {backgroundColor: this.props.backgroundColor}]}>
                    <View style = {[styles.shadowView, {backgroundColor: `rgba(0, 0, 0, ${this.state.opacity})`}]}>
                        {this.title()}
                        {this.image()}
                    </View>
                </View>
            </TouchableWithoutFeedback>
        )
    }
}

const styles = StyleSheet.create({
    buttonView: {
        alignItems: 'center',
        justifyContent: 'center'
    },

    shadowView: {
        alignSelf: "stretch", 
        flex: 1, 
        justifyContent: 'center',
        alignItems: 'center'
    }
})

export default PinCodeButton