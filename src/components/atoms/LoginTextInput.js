import React from 'react'
import {px2dp} from '../../utils/utils'
import * as colors from '../../constants/Colors'
import * as images from '../../constants/Images'
import { 
    View,
    TextInput,
    Text, 
    StyleSheet, 
    Image,
    Platform
} from 'react-native'

class LoginTextInput extends React.Component {
    headerTitle() {
        return <Text style = {styles.headerTitle}>
            {this.props.inputHeader}
        </Text>
    }

    textInput() {
        return <TextInput
            style = {[styles.textInput, styles.textInputWithImage]}
            autoCapitalize = "none"
            autoCorrect = {false}
            keyboardType = {this.props.keyboardType}
            placeholder = {this.props.placeholder}
            placeholderTextColor = {colors.POLO_BLUE}
            secureTextEntry = {this.props.secureTextEntry}
            underlineColorAndroid = "transparent"
            
            onChangeText = {(text) => {
                this.props.onChangeText(text)
            }}

            onEndEditing = {this.props.onEndEditing}
        />
    }

    errorImage() {
        if (this.props.errorText) {
            return <Image
                style = {styles.textInputWithImage}
                source = {images.error}
            />
        }

        return null
    }

    viewWithTextInput() {
        return <View style = {styles.viewWithTextInput}>
            {this.textInput()}
            {this.errorImage()}
        </View>
    }

    underlineView() {
        return <View 
            style = {this.props.errorText ? styles.underlineErrorView : styles.underlineView}>
        </View>
    }

    errorTitle() {
        if (this.props.errorText) {
            return <Text style = {styles.errorTitle}>
                {this.props.errorText}
            </Text>
        }

        return null;
    }

    render() {
        return(
            <View style = {this.props.style}>
                {this.headerTitle()}
                {this.viewWithTextInput()}
                {this.underlineView()}
                {this.errorTitle()}
            </View>
        )
    }
}

const styles = StyleSheet.create ({
    headerTitle: {
        color : colors.JUNGLE_MIST,
        fontSize: px2dp(22)
    },

    textInputWithImage: {
        marginTop: (Platform.OS === 'ios') ? px2dp(6) : -px2dp(25)
    },

    textInput: {
        marginLeft: (Platform.OS === 'ios') ? 0 : -px2dp(10),
        fontSize: px2dp(28),
        flexGrow: 1,
        color: colors.CURIOUS_BLUE
    },

    underlineView: {
        height: 1,
        backgroundColor: colors.ONAHAU,
        marginTop: (Platform.OS === 'ios') ? px2dp(15) : -px2dp(15)
    },

    underlineErrorView: {
        height: 1,
        backgroundColor: colors.TOMATO,
        marginTop: (Platform.OS === 'ios') ? px2dp(15) : -px2dp(15)
    },

    errorTitle: {
        color: colors.TOMATO,
        fontSize: px2dp(20),
        marginTop: px2dp(7.5)
    },

    viewWithTextInput: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    }
})

export default LoginTextInput