import React from 'react'
import {px2dp} from '../../utils/utils'
import * as colors from '../../constants/Colors'
import { 
    TouchableWithoutFeedback,
    Text,
    View,
    StyleSheet
} from 'react-native'

class ZoneCell extends React.Component {
    zoneCell() {
        return <View style = {[
                    this.props.style,
                    styles.zoneCell,
                    {backgroundColor: this.props.selected ? colors.PATTENS_BLUE : 'transparent'}
        ]}>
            {this.title()}
            {this.separatorLine()}
        </View>
    }

    title() {
        return <Text style = {[styles.title, {color: this.props.selected ? colors.CURIOUS_BLUE : 'black'}]}>
            {this.props.title}
        </Text>
    }

    separatorLine() {
        return <View
            style = {styles.separatorLine}
        />
    }

    render() {
        return(
            <TouchableWithoutFeedback
                onPress = {this.props.onPress}
            >
                {this.zoneCell()}
            </TouchableWithoutFeedback>
        )
    }
}

const styles = StyleSheet.create({
    zoneCell: {
        flexDirection: 'row',
        alignItems: 'center'
    },

    title: {
        marginLeft: px2dp(70),
        marginRight: px2dp(70),
        fontSize: px2dp(28),
        fontWeight: '500'
    },

    separatorLine: {
        width: px2dp(2),
        backgroundColor: 'white',
        alignSelf: 'stretch'
    }
})

export default ZoneCell