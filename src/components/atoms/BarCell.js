import React from 'react'
import {px2dp} from '../../utils/utils'
import * as colors from '../../constants/Colors'
import {
    View,
    Text,
    TouchableWithoutFeedback,
    StyleSheet
} from 'react-native'

class BarCell extends React.Component {
    borderColor() {
        return {borderColor: this.props.selected ? colors.PATTENS_BLUE : colors.WHISPER}
    }

    titleColor() {
        return {color: this.props.selected ? colors.CURIOUS_BLUE : colors.STORM_GREY}
    }

    barCell() {
        return <TouchableWithoutFeedback
            onPress = {this.props.onPress}
        >
            <View style = {[styles.barCell, this.borderColor(), this.props.style]}>
                <Text style = {[styles.title, this.titleColor()]}>
                    {this.props.title}
                </Text>
            </View>
        </TouchableWithoutFeedback>
    }

    render() {
        return this.barCell()
    }
}

const verticalOffset = px2dp(15)
const horizontalOffset = px2dp(50)

const styles = StyleSheet.create({
    barCell: {
        borderWidth: 1
    },

    title: {
        fontSize: px2dp(26),
        marginTop: verticalOffset,
        marginBottom: verticalOffset,
        marginLeft: horizontalOffset,
        marginRight: horizontalOffset
    }
})

export default BarCell