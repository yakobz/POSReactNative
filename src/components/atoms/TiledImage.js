import React from 'react'
import * as images from '../../constants/Images'
import {
    View,
    Image,
    StyleSheet
} from 'react-native'

class TiledImage extends React.Component {
    images = []
    imageWidth = 5

    constructor(props) {
        super(props)

        for (var j = 0; j < Math.ceil(2 * props.height / this.imageWidth); j++) {
            for (var i = 0; i < Math.ceil(props.width / this.imageWidth); i++) {
                this.images.push((
                    <Image source = {images.shortline} 
                           style = {{width: this.imageWidth, height: this.imageWidth}}
                    />
                ))
            }
        }
    }

    render() {
        return <View style = {[styles.tiledImage, {width: this.props.width + 2 * this.imageWidth}]}>
            {
                this.images.map((image) => {
                    return image
                })
            }
        </View>
    }
}

const styles = StyleSheet.create({
    tiledImage: {
        position: 'absolute',
        flexDirection: 'row',
        flexWrap: 'wrap'
    }
})

export default TiledImage