import React from 'react'
import {px2dp} from '../../utils/utils'
import * as images from '../../constants/Images'
import {
    View,
    Image,
    Text,
    StyleSheet
} from 'react-native'

class TableBusyTimeView extends React.Component {
    timeView() {
        return <View style = {styles.timeView}>
            {this.timeImage()}
            {this.timeTitle()}
        </View>
    }

    timeImage() {
        return <Image
            source = {images.clocks}
        />
    }

    timeTitle() {
        return <Text style = {styles.title}>
            {this.props.time}
        </Text>
    }

    render() {
        return this.timeView()
    }
}

const styles = StyleSheet.create({
    timeView: {
        flexDirection: 'row',
        alignItems: 'center'
    },

    title: {
        color: 'white',
        marginLeft: px2dp(20),
        fontSize: px2dp(26),
        fontWeight: '600'
    }
})

export default TableBusyTimeView