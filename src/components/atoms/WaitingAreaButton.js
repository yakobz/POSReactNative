import React from 'react'
import * as colors from '../../constants/Colors'
import * as images from '../../constants/Images'
import {px2dp} from '../../utils/utils'
import {
    View,
    TouchableWithoutFeedback,
    Image,
    Text,
    StyleSheet
} from 'react-native'

class WaitingAreaButton extends React.Component {
    backgroundColor() {
        return {backgroundColor: this.props.enable ? colors.PATTENS_BLUE_WAITING_AREA : 'white'}
    }

    border() {
        if (this.props.enable === false) {
            return {
                borderColor: colors.WHITE_SMOKE,
                borderWidth: 1
            }
        }
    }

    imageSource() {
        if (this.props.type === 'delivery') {
            return this.props.enable ? images.deliveryactive : images.delivery
        } else {
            return this.props.enable ? images.ordericon : images.ordernotactive
        }
    }

    image() {
        return <Image
            style = {styles.image}
            source = {this.imageSource()}
        />
    }

    titleColor() {
        return {color: this.props.enable ? colors.SWAMP : colors.SILVER}
    }

    title() {
        return <Text style = {[styles.title, this.titleColor()]}>
            {this.props.title}
        </Text>
    }

    waitingAreaButton() {
        return <TouchableWithoutFeedback
            onPress = {this.props.onPress}
        >
            <View style = {[this.props.style, styles.waitingAreaButton, this.backgroundColor(), this.border()]}>
                {this.image()}
                {this.title()}
            </View>
        </TouchableWithoutFeedback>
    }

    render() {
        return this.waitingAreaButton()
    }
}

const styles = StyleSheet.create({
    waitingAreaButton: {
        width: px2dp(180),
        height: px2dp(100),
        borderRadius: 8,
        flexDirection: 'row',
        alignItems: 'center'
    },

    image: {
        marginLeft: px2dp(25)
    },

    title: {
        fontSize: px2dp(34),
        marginLeft: px2dp(30)
    }
})

export default WaitingAreaButton