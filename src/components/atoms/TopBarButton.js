import React from 'react'
import {px2dp} from '../../utils/utils'
import * as colors from '../../constants/Colors'
import { 
    TouchableWithoutFeedback,
    Text,
    View,
    StyleSheet,
    Platform
} from 'react-native'

class TopBarButton extends React.Component {
    button() {
        return <TouchableWithoutFeedback
            onPress = {this.props.onPress}
            >
                {this.buttonContent()}
        </TouchableWithoutFeedback>
    }

    buttonContent() {
        return <View style = {this.props.style}>
            {this.buttonTitle()}
            {this.underline()}    
        </View>
    }

    buttonTitle() {
        return <Text style = {[styles.buttonTitle, {color: this.props.selected ? colors.CURIOUS_BLUE : 'black'}]}>
            {this.props.title}
        </Text>
    }

    underline() {
        if (this.props.selected) {
            return <View style = {styles.underline}>
            </View>
        }

        return null
    }

    render() {
        return(
            this.button()
        )
    }
}

const styles = StyleSheet.create({
    buttonTitle: {
        fontSize: px2dp(26),
        fontWeight: '600',
        paddingLeft: Platform.OS === 'ios' ? 0 : px2dp(20),
        paddingRight: Platform.OS === 'ios' ? 0 : px2dp(20)
    },

    underline: {
        marginTop: px2dp(16),
        marginLeft: Platform.OS === 'ios' ? px2dp(-20) : 0,
        marginRight: Platform.OS === 'ios' ? px2dp(-20) : 0,
        height: px2dp(6),
        backgroundColor: colors.CURIOUS_BLUE,
    }
})

export default TopBarButton