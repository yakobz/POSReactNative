import React from 'react'
import * as colors from '../../constants/Colors'
import * as images from '../../constants/Images'
import {px2dp} from '../../utils/utils'
import {
    View,
    Text,
    TouchableWithoutFeedback,
    Image,
    StyleSheet
} from 'react-native'

class ChairCell extends React.Component {
    backgroundColor() {
        return {backgroundColor: this.props.chairState === 'busy' ? colors.NEON_CARROT : colors.CARIBBEAN_GREEN}
    }

    chairCell() {
        return <View style = {styles.mainView}>
            <View style = {[styles.chairCell, this.backgroundColor()]}>
                {this.title()}
            </View>
            {this.bookingImage()}
        </View>
    }

    title() {
        return <Text style = {styles.title}>
            {this.props.title}
        </Text>
    }

    bookingImage() {
        return <TouchableWithoutFeedback
            onPress = {this.props.bookingButtonPressed}
        >
            <Image
                style = {styles.bookingImage}
                source = {images.booking}
            />
        </TouchableWithoutFeedback>
    }

    render() {
        return <TouchableWithoutFeedback
            onPress = {this.props.onPress}
        >
            {this.chairCell()}
        </TouchableWithoutFeedback>
    }
}

const width = px2dp(100)

const styles = StyleSheet.create({
    mainView: {
        width: 1.2 * width,
        height: 1.5 * width,
        alignItems: 'center',
        justifyContent: 'center'
    },

    chairCell: {
        width: width,
        height: width,
        borderRadius: 0.5 * width,
        justifyContent: 'center',
        alignItems: 'center'
    },

    title: {
        fontSize: px2dp(32),
        fontWeight: 'bold',
        color: 'white'
    },

    bookingImage: {
        position: 'absolute',
        width: 0.5 * width,
        height: 0.5 * width,
        left: 0.75 * width,
        bottom: 0.85 * width
    }
})

export default ChairCell