import React from 'react'
import TopBarButton from '../atoms/TopBarButton'
import {px2dp} from '../../utils/utils'
import { 
    View,
    StyleSheet,
} from 'react-native'

buttonTitles = ["Tables", "Bookings", "Delivery", "Check in"]

class TopBarButtons extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            selectedButtonIndex: 0
        }
    }

    buttonWithTitle(title, selected, index) {
        return <TopBarButton
            key = {index}
            style = {styles.button}
            title = {title}
            selected = {selected}
            onPress = {() => {
                this.setState({
                    selectedButtonIndex: index
                })
            }}
        />
    }

    render() {
        return (
            <View style = {[this.props.style, styles.buttons]}>
                {
                    buttonTitles.map((title, index) => (
                        this.buttonWithTitle(title, index === this.state.selectedButtonIndex, index)
                    ))
                }
            </View>
        )
    }
}

const styles = StyleSheet.create ({
    buttons: {
        flexDirection: 'row',
        alignItems: 'flex-start',
    },

    button: {
        marginLeft: px2dp(35),
        marginRight: px2dp(35)
    }
});

export default TopBarButtons