import React from 'react'
import ChairCell from '../atoms/ChairCell'
import * as images from '../../constants/Images'
import {px2dp} from '../../utils/utils'
import LinearGradient from 'react-native-linear-gradient'
import {
    View,
    Image,
    FlatList,
    Alert,
    TouchableWithoutFeedback,
    StyleSheet
} from 'react-native'

class BarChairsView extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            hideTopArrow: true,
            hideBottomArrow: true
        }
    }

    componentDidMount() {
        setTimeout(() => {
            this.chairs.scrollToOffset({offset: 1, animated: true})
        }, 0);
    }

    chairsData() {
        var chairsData = []

        for (var i = 0; i < 10; i++) {
            chairsData.push({key: i, title: `${i}`, state: i % 2 === 0 ? 'busy' : 'free'})
        }

        return chairsData
    }

    handleScroll(nativeEvent) {
        scrollViewHeight = nativeEvent.layoutMeasurement.height

        if (nativeEvent.contentSize.height > scrollViewHeight) {
            hideTopArrow = this.state.hideTopArrow
            hideBottomArrow = this.state.hideBottomArrow
            bottomSpace = nativeEvent.contentSize.height - nativeEvent.contentOffset.y
    
            if (nativeEvent.contentOffset.y > offset && (hideTopArrow === true)) {
                hideTopArrow = false
            } else if (nativeEvent.contentOffset.y < offset && (hideTopArrow === false)) {
                hideTopArrow = true
            }

            if (bottomSpace > scrollViewHeight + 2 * offset && (hideBottomArrow === true)) {
                hideBottomArrow = false
            } else if (bottomSpace < scrollViewHeight + 2 * offset && (hideBottomArrow === false)) {
                hideBottomArrow = true
            }

            if (hideBottomArrow !== this.state.hideBottomArrow || hideTopArrow !== this.state.hideTopArrow) {
                this.setState({
                    hideBottomArrow: hideBottomArrow,
                    hideTopArrow: hideTopArrow
                })
            }
        }
    }

    topImage() {
        return <TouchableWithoutFeedback
            onPress = {() => {
                this.chairs.scrollToOffset({offset: 0, animated: true})
            }}
        >
            <Image
                source = {this.state.hideTopArrow ? images.arrowup_notactive : images.arrowup_active}
            />
        </TouchableWithoutFeedback>
    }

    bottomImage() {
        return <TouchableWithoutFeedback
            onPress = {() => {
                this.chairs.scrollToEnd()
            }}
        >
            <Image
                source = {this.state.hideBottomArrow ? images.arrowdown_notactive : images.arrowdown_active}
            />
        </TouchableWithoutFeedback>
    }

    chairsFlatList() {
        return <FlatList
            ref = {(chairs) => {this.chairs = chairs}}
            contentContainerStyle = {styles.chairsContainer}
            showsVerticalScrollIndicator = {false}
            style = {styles.chairs}
            data = {this.chairsData()}
            renderItem = {({item}) => 
                <ChairCell
                    title = {item.title}
                    chairState = {item.state}
                    onPress = {() => {
                        Alert.alert(`ahahaha chair with number: ${item.title}`)
                    }}
                    bookingButtonPressed = {() => {
                        Alert.alert(`bookingButton pressed in ${item.title}`)
                    }}
                />
            }
            onScroll = {(event) => {
                this.handleScroll(event.nativeEvent)
            }}
        />
    }

    gradients() {
        return <View
            style = {styles.gradientsView}
            pointerEvents = "none"
        >
            <LinearGradient
                colors = {['rgba(255, 255, 255, 1)', 'rgba(255, 255, 255, 0)']}
                style = {[styles.gradient, {marginTop: gradientOffset}]}
                locations = {[0.3, 1]}
            />
            <LinearGradient
                colors = {['rgba(255, 255, 255, 0)', 'rgba(255, 255, 255, 1)']}
                style = {[styles.gradient, {marginBottom: gradientOffset}]}
                locations = {[0, 0.7]}
            />
        </View>
    }

    barChairsView() {
        return <View style = {[styles.barChairsView, this.props.style]} >
            {this.topImage()}
            {this.chairsFlatList()}
            {this.bottomImage()}
            {this.gradients()}
        </View>
    }

    render() {
        return this.barChairsView()
    }
}

const offset = px2dp(35)
const chairsOffset = px2dp(20)
const gradientOffset = 8 + chairsOffset

const styles = StyleSheet.create({
    barChairsView: {
        alignItems: 'center'
    },

    chairs: {
        width: '100%',
        marginTop: chairsOffset,
        marginBottom: chairsOffset
    },

    chairsContainer: {
        alignItems: 'center',
        paddingBottom: offset,
    },
    
    gradientsView: {
        position: 'absolute',
        width: '100%',
        height: '100%',
        justifyContent: 'space-between'
    },

    gradient: {
        width: '100%',
        height: px2dp(50)
    }
})

export default BarChairsView