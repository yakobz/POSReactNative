import React from 'react'
import ButtonWithOpacityOnPress from '../atoms/ButtonWithOpacityOnPress'
import {px2dp} from '../../utils/utils'
import * as colors from '../../constants/Colors'
import { 
    View,
    StyleSheet,
    TouchableOpacity,
    Text
} from 'react-native'

class LoginButtons extends React.Component {
    loginButton() {
        return <ButtonWithOpacityOnPress
            backgroundColor = {colors.CURIOUS_BLUE}
            textColor = 'white'
            fontSize = {px2dp(30)}
            style = {styles.blueButton}
            title = "Sign in"
            onPress = {this.props.signInButtonPressed}
        />
    }

    bottomButtonWithTitle(title) {
        return <TouchableOpacity>
            <Text style = {styles.bottomButtonTitle}>{title}</Text>
        </TouchableOpacity>
    }

    bottomButtons() {
        return <View style = {styles.bottomButtons}>
            {this.bottomButtonWithTitle("Registration")}
            {this.bottomButtonWithTitle("Forgot password?")}
        </View>
    }

    render() {
        return (
            <View style = {this.props.style}>
                {this.loginButton()}
                {this.bottomButtons()}
            </View>
        )
    }
}

const styles = StyleSheet.create ({
    blueButton: {
        width: px2dp(555),
        height: px2dp(85)
    },

    bottomButtons: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: px2dp(30)
    },

    bottomButtonTitle: {
        color: colors.CURIOUS_BLUE,
        fontSize: px2dp(27)
    }
});

export default LoginButtons