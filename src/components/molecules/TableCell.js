import React from 'react'
import TableNumberView from '../atoms/TableNumberView'
import TableNumberOfGuestsView from '../atoms/TableNumberOfGuestsView'
import TableBusyTimeView from '../atoms/TableBusyTimeView'
import {px2dp} from '../../utils/utils'
import * as colors from '../../constants/Colors'
import {
    View,
    Image,
    TouchableWithoutFeedback,
    StyleSheet
} from 'react-native'

const tableCellSize = px2dp(226)
const tableCellHorizontalOffset = px2dp(45)
const tableCellVerticalOffset = px2dp(77)

class TableCell extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            tableState: props.state
        }
    }

    backgroundColor() {
        return {backgroundColor: this.state.tableState === 'busy' ? colors.NEON_CARROT : colors.CARIBBEAN_GREEN}
    }

    tableNumberView() {
        return <TableNumberView
            style = {{marginTop: px2dp(20)}}
            tableState = {this.state.tableState}
            title = {this.props.title}
        />
    }

    tableBusyTimeView() {
        if (this.state.tableState === 'busy') {
            return <TableBusyTimeView
                time = "05:30"
            />
        }

        return null
    }

    numberOfGuestsView() {
        return <TableNumberOfGuestsView
            style = {{marginBottom: px2dp(20)}}
            tableState = {this.state.tableState}
            numberOfGuests = {4}
        />
    }

    bookingImage() {
        if (this.props.bookingImage !== null) {
            return <TouchableWithoutFeedback
                onPress = {() => {
                    this.props.bookingButtonPressed(this.props.title)
                }}
            >
                <Image
                    style = {styles.bookingImage}
                    source = {this.props.bookingImage}
                />
            </TouchableWithoutFeedback>
        }

        return null
    }

    render() {
        return (
            <TouchableWithoutFeedback
                onPress = {() => {
                    this.props.onPress(this.props.title)
                }}
            >
                <View>
                    <View style = {[styles.tableCell, this.props.style, this.backgroundColor()]}>
                        {this.tableNumberView()}
                        {this.tableBusyTimeView()}
                        {this.numberOfGuestsView()}
                    </View>
                    {this.bookingImage()}
                </View>
            </TouchableWithoutFeedback>
        )
    }
}

const styles = StyleSheet.create({
    tableCell: {
        width: tableCellSize,
        height: tableCellSize,
        marginLeft: tableCellHorizontalOffset,
        marginRight: tableCellHorizontalOffset,
        marginTop: tableCellVerticalOffset,
        borderRadius: 10,
        justifyContent: 'space-between',
        alignItems: 'center',
    },

    bookingImage: {
        position: 'absolute',
        left: tableCellSize,
        top: 0.5 * tableCellVerticalOffset
    }
})

export default TableCell