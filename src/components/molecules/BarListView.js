import React from 'react'
import BarCell from '../atoms/BarCell'
import {px2dp} from '../../utils/utils'
import {
    View,
    FlatList,
    Alert,
    StyleSheet
} from 'react-native'

class BarListView extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            selectedBarIndex: 0
        }
    }

    barCellData() {
        var data = []

        for (var i = 0; i < this.props.barsCount; i++) {
            data.push({key: i, title: `bar_${i}`, selected: i === this.state.selectedBarIndex})
        }

        return data
    }

    barCellStyleWithIndex(index) {
        // borderRadius = px2dp(10)
        borderRadius = 5

        return {
            borderBottomLeftRadius: index === 0 ? borderRadius : 0,
            borderTopLeftRadius: index === 0 ? borderRadius : 0,
            borderBottomRightRadius: index === this.props.barsCount - 1 ? borderRadius : 0,
            borderTopRightRadius: index === this.props.barsCount - 1 ? borderRadius : 0
        }
    }

    barListView() {
        return <View style = {[styles.barListView, this.props.style]}>
            <FlatList
                horizontal = {true}
                showsHorizontalScrollIndicator = {false}
                data = {this.barCellData()}
                renderItem = {({item, index}) => {
                    return <BarCell
                    selected = {item.selected}
                    style = {this.barCellStyleWithIndex(index)}
                    title = {item.title}
                    onPress = {() => {
                        this.setState({
                            selectedBarIndex: index
                        })
                    }}
                />
                }}
            />
        </View>
    }

    render() {
        return this.barListView()
    }
}

const horizontalOffset = px2dp(45)

const styles = StyleSheet.create({
    barListView: {
        marginTop: px2dp(30),
        marginLeft: horizontalOffset,
        marginRight: horizontalOffset
    }
})

export default BarListView