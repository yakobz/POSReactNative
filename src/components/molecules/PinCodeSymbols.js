import React from 'react'
import PinCodeSymbol from '../atoms/PinCodeSymbol'
import {px2dp} from '../../utils/utils'
import { 
    View,
    StyleSheet
} from 'react-native'

class PinCodeSymbols extends React.Component {
    render() {
        return(
            <View style = {[this.props.style, styles.pinCodeSymbols]}>
                <PinCodeSymbol filled = {this.props.pin.length >= 1}/>
                <PinCodeSymbol filled = {this.props.pin.length >= 2}/>
                <PinCodeSymbol filled = {this.props.pin.length >= 3}/>
                <PinCodeSymbol filled = {this.props.pin.length == 4}/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    pinCodeSymbols: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginLeft: px2dp(135),
        marginRight: px2dp(135)
    }
})

export default PinCodeSymbols