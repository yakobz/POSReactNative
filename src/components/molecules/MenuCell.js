import React from 'react'
import {px2dp} from '../../utils/utils'
import * as colors from '../../constants/Colors'
import {
    View,
    Image,
    Text,
    TouchableWithoutFeedback,
    StyleSheet
} from 'react-native'

class MenuCell extends React.Component {
    backgroundView() {
        if (this.props.selected) {
            return <View style = {styles.backgroundView} />
        }

        return null;
    }

    leftLine() {
        if (this.props.selected) {
            return <View style = {styles.leftline} />
        }

        return null;
    }

    menuCell() {
        return <TouchableWithoutFeedback
            onPress = {this.props.onPress}
        >
            <View style = {[styles.menuCell, this.props.style]}>
                {this.leftLine()}
                {this.backgroundView()}
                {this.image()}
                {this.title()}
                {this.bottomLine()}
            </View>
        </TouchableWithoutFeedback>
    }

    image() {
        return <Image
            source = {this.props.selected ? this.props.selectedImage : this.props.image}
            style = {styles.image}
        />
    }

    title() {
        return <Text style = {[this.props.selected ? styles.selectedTitle : styles.unselectedTitle, styles.title]}>
            {this.props.title}
        </Text>
    }

    bottomLine() {
        return <View style = {styles.bottomLine}>
        </View>
    }

    render() {
        return this.menuCell()
    }
}

const leftLineWidth = px2dp(10)
const horizontalOffset = px2dp(25)

const styles = StyleSheet.create({
    menuCell: {
        height: px2dp(110),
        flexDirection: 'row',
        alignItems: 'center'
    },

    title: {
        fontSize: px2dp(30),
        marginLeft: px2dp(35)        
    },

    unselectedTitle: {
        fontWeight: '400',
        color: colors.CHAMBRAY,
    },

    selectedTitle: {
        fontWeight: 'bold',
        color: colors.CURIOUS_BLUE,
        backgroundColor: 'transparent'
    },

    bottomLine: {
        position: 'absolute',
        height: px2dp(3),
        width: '100%',
        backgroundColor: colors.WHITE_SMOKE_LINE,
        marginTop: px2dp(126),
        alignSelf: 'flex-end',
        marginLeft: horizontalOffset
    },

    backgroundView: {
        position: 'absolute',
        backgroundColor: colors.WHITE_SMOKE_FILL,
        width: '110%',
        height: '100%',
        marginLeft: leftLineWidth
    },

    leftline: {
        position: 'absolute',
        height: '100%',
        width: leftLineWidth,
        backgroundColor: colors.CURIOUS_BLUE,
    },

    image: {
        marginLeft: horizontalOffset
    }
})

export default MenuCell