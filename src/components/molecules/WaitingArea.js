import React from 'react'
import WaitingAreaButton from '../atoms/WaitingAreaButton'
import AddQuickOrderBarButton from '../atoms/AddQuickOrderBarButton'
import * as images from '../../constants/Images'
import {px2dp} from '../../utils/utils'
import {
    View,
    Alert,
    Dimensions,
    StyleSheet
} from 'react-native'

class WaitingArea extends React.Component {
    waitingOrdersButton() {
        return <WaitingAreaButton
            enable = {false}
            title = "500"
            onPress = {() => {
                Alert.alert("waitingOrdersButton")
            }}
        />
    }

    waitingDeliveryButton() {
        return <WaitingAreaButton
            enable = {true}
            title = "0"
            type = 'delivery'
            onPress = {() => {
                Alert.alert("waitingDeliveryButton")
            }}
        />
    }

    addOrderButton() {
        return <AddQuickOrderBarButton
            style = {{marginLeft: px2dp(30), marginRight: px2dp(30), backgroundColor: 'white'}}
            width = {px2dp(115)}
            offset = {px2dp(20)}
            height = {0.2 * Dimensions.get("window").height}
            image = {images.plusBlue}
            onPress = {() => {
                Alert.alert("AddQuickOrderBarButton")
            }}
        />
    }

    waitingArea() {
        return <View style = {[styles.waitingArea, this.props.style]}>
            <View style = {styles.waitingButtons}>
                {this.waitingOrdersButton()}
                {this.waitingDeliveryButton()}
            </View>
            {this.addOrderButton()}
        </View>
    }

    render() {
        return this.waitingArea()
    }
}

const styles = StyleSheet.create({
    waitingArea: {
        backgroundColor: 'white',
        borderTopRightRadius: 10,
        width: '85%',
        flexDirection: 'row',
        shadowColor: 'black',
        shadowOffset: {width: 0, height: 1},
        shadowOpacity: 0.1,
        shadowRadius: 5,
        elevation: 10
    },

    waitingButtons: {
        marginLeft: px2dp(20),
        justifyContent: 'space-around'        
    }
})

export default WaitingArea