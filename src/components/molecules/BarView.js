import React from 'react'
import BarQuickOrdersView from '../atoms/BarQuickOrdersView'
import AddQuickOrderBarButton from '../atoms/AddQuickOrderBarButton'
import {px2dp} from '../../utils/utils'
import * as colors from '../../constants/Colors'
import * as images from '../../constants/Images'
import {
    View,
    Alert,
    Dimensions,
    StyleSheet
} from 'react-native'

class BarView extends React.Component {
    backgroundColor() {
        return {backgroundColor: this.props.ordersCount > 0 ? colors.TANGERINE :  colors.PERSIAN_GREEN}
    }

    addQuickOrderBackgroundColor() {
        return {backgroundColor: this.props.ordersCount > 0 ? colors.SUNSHADE : colors.CARIBBEAN_GREEN_BUTTON}
    }

    addQuickOrderImage() {
        return this.props.ordersCount > 0 ? images.plusOrange : images.greenplus
    }

    barQuickOrdersView() {
        if (this.props.ordersCount > 0) {
            return <BarQuickOrdersView
                ordersCount = {this.props.ordersCount}
                onPress = {() => {
                    Alert.alert("BarQuickOrdersView")
                }}
            />
        }

        return null
    }

    addAuickOrderBarButton() {
        return <AddQuickOrderBarButton
            width = {px2dp(100)}
            offset = {px2dp(15)}
            height = {0.5 * Dimensions.get("window").height}
            style = {this.addQuickOrderBackgroundColor()}
            image = {this.addQuickOrderImage()}
            onPress = {() => {
                Alert.alert("AddQuickOrderBarButton")
            }}
        />
    }

    barView() {
        return <View style = {[this.props.style, styles.barView, this.backgroundColor()]}>
            {this.barQuickOrdersView()}
            {this.addAuickOrderBarButton()}
        </View>
    }

    render() {
        return this.barView()
    }
}

const styles = StyleSheet.create({
    barView: {
        alignItems: 'center',
        borderRadius: 10
    }
})

export default BarView