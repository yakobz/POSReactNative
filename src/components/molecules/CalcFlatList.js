import React from 'react'
import PinCodeButton from '../atoms/PinCodeButton'
import { px2dp } from '../../utils/utils'
import * as colors from '../../constants/Colors'
import { 
    FlatList
} from 'react-native'

class CalcFlatList extends React.Component {
    constructor(props) {
        super(props)
        rowWidth = 0
        inset = 0
    }

    componentWillMount() {
        this.rowWidth = 0.33 * this.props.rowWidth
        this.inset = 0.005 * this.props.rowWidth
    }

    calcView() {
        return <FlatList
            scrollEnabled = {false}
            showsVerticalScrollIndicator = {false}
            numColumns = {this.props.columnsNumber}
            data = {this.props.data}
            renderItem = {
                ({item, index}) => <PinCodeButton
                    style = {{
                        width: this.rowWidth, 
                        height: 0.5 * this.rowWidth, 
                        marginTop: index < this.props.columnsNumber ? 0 : this.inset, 
                        marginLeft: index % this.props.columnsNumber == 0 ? 0 : this.inset,
                        borderBottomLeftRadius: index == 9 ? 20 : 0,
                        borderBottomRightRadius: index == 11 ? 20 : 0
                    }}
                    title = {item.title}
                    image = {item.image}
                    fontSize = {item.fontSize ? item.fontSize : px2dp(52)}
                    backgroundColor = {colors.ALICE_BLUE}
                    textColor = {item.textColor}
                    fontWeight = {item.fontWeight}
                    onPress = {() => {
                        this.props.onPress(item.key)
                    }}
                />
            }
        />

    }

    render() {
        return (
            this.calcView()
        )
    }
}

export default CalcFlatList