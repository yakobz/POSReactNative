import React from 'react'
import TableCell from '../../molecules/TableCell'
import {px2dp} from '../../../utils/utils'
import * as images from '../../../constants/Images'
import { 
    View,
    ScrollView,
    Alert,
    StyleSheet,
} from 'react-native'

class TablesView extends React.Component {
    bookingImage(index) {
        if (index % 3 === 1) {
            return images.booking
        } else if (index % 3 === 2) {
            return images.booking_soon
        }

        return null
    }

    render() {
        var tables = []

        for (var i = 0; i < 58; i++) {
            tables.push(
                <TableCell
                    state = {i % 2 === 0 ? 'free' : 'busy'}
                    title = {`${i}`}
                    bookingImage = {this.bookingImage(i)}
                    onPress = {(title) => {
                        Alert.alert(`table cell pressed: ${title}`)
                    }}
                    bookingButtonPressed = {(title) => {
                        Alert.alert(`table cell booking button pressed: ${title}`)
                        
                    }}
                />
            )
        }

        return <ScrollView
            showsVerticalScrollIndicator = {false}
            style = {this.props.style}
            contentContainerStyle = {styles.scrollViewContainer}
        >
            <View style = {styles.tablesView} >
                {tables}
            </View>
        </ScrollView>
    }
}

const styles = StyleSheet.create ({
    tablesView: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-around'
    },

    scrollViewContainer: {
        paddingBottom: px2dp(77)
    }
});

export default TablesView