import React from 'react'
import BarView from '../../molecules/BarView'
import BarChairsView from '../../molecules/BarChairsView'
import WaitingArea from '../../molecules/WaitingArea'
import {px2dp} from '../../../utils/utils'
import {
    View,
    StyleSheet
} from 'react-native'

class BarAndWaitingAreaView extends React.Component {
    barView() {
        return <BarView
            style = {styles.barView}
            ordersCount = {10}
        />
    }

    barAndWaitingAreaView() {
        return <View style = {this.props.style}>
            <View style = {styles.barAndChairsView}> 
                {this.barView()}
                <BarChairsView style = {styles.barChairsView} />
            </View>
            <WaitingArea style = {styles.waitingArea}/>
        </View>
    }

    render() {
        return this.barAndWaitingAreaView()
    }
}

const marginTop = px2dp(77)
const marginBottom = px2dp(90)

const styles = StyleSheet.create({
    barAndChairsView: {
        flex: 1,
        flexDirection: 'row'
    },

    barView: {
        flex: 0.35,
        marginTop: marginTop,
        marginLeft: px2dp(48),
        marginBottom: marginBottom
    },

    barChairsView: {
        flex: 0.38,
        marginTop: marginTop,
        marginLeft: px2dp(30),
        marginBottom: marginBottom
    },

    waitingArea: {
        flex: 0.27
    }
})

export default BarAndWaitingAreaView