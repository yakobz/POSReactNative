import React from 'react'
import ZoneCell from '../../atoms/ZoneCell'
import * as colors from '../../../constants/Colors'
import { 
    FlatList,
    StyleSheet,
    Dimensions
} from 'react-native'

zones = [
    {key: 1, title: "First floor"},
    {key: 2, title: "Second floor"},
    {key: 3, title: "Balcony"},
    {key: 4, title: "Terrace"}
]

class ZonesView extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            selectedZoneIndex: 0
        }
    }

    render() {
        return <FlatList
            horizontal = {true}
            showsVerticalScrollIndicator = {false}
            showsHorizontalScrollIndicator = {false}
            style = {[this.props.style, styles.zoneView]}
            data = {zones}
            renderItem = {
                ({item, index}) => <ZoneCell
                    style = {{flex: 1}}
                    title = {item.title}
                    selected = {index === this.state.selectedZoneIndex}
                    onPress = {() => {
                        this.setState({
                            selectedZoneIndex: index
                        })
                    }}
                />
            }
        />
    }
}

const styles = StyleSheet.create ({
    zoneView: {
        flex: 1,
        backgroundColor: colors.WHISPER,
        width: Dimensions.get("window").width
    }
});

export default ZonesView