import React from 'react'
import TopBarButtons from '../../molecules/TopBarButtons'
import {px2dp} from '../../../utils/utils'
import * as images from '../../../constants/Images'
import { 
    View,
    Image,
    StatusBar,
    TouchableOpacity,
    StyleSheet,
    Dimensions
} from 'react-native'

class CustomNavigationBar extends React.Component {
    leftButton() {
        return <TouchableOpacity
                onPress = {this.props.leftBarButtonPressed}
            >
            <Image
                style = {styles.leftButton}
                source = {images.menu_icon}
            />
        </TouchableOpacity>
    }

    rightButtons() {
        return <TopBarButtons
        />
    }

    render() {
        return (
                <View style = {{zIndex: 100}}>
                    <StatusBar />
                    <View style = {[styles.navigationBar, this.props.style]} >
                        {this.leftButton()}
                        {this.rightButtons()}
                    </View>
                </View>
        )
    }
}

const styles = StyleSheet.create ({
    navigationBar: {
        flexDirection: 'row',
        width: Dimensions.get("window").width,
        height: px2dp(127),
        alignItems:'flex-end',
        justifyContent: 'space-between',
        shadowOffset: {width: 5, height: 0},
        shadowColor: 'black',
        shadowOpacity: 0.3,
        shadowRadius: 5,
        elevation: 10
    },

    leftButton: {
        width: px2dp(35),
        height: px2dp(30),
        marginLeft: px2dp(48),
        marginBottom: px2dp(28)
    }
});

export default CustomNavigationBar