import { connect } from 'react-redux'
import actions from '../../../actions/menu'
import Menu from './Menu'

const mapStatetToProps = store => ({
    selectedItemIndex: store.menu.selectedItemIndex,
    items: store.menu.items
})

const mapDispatchToProps = dispatch => ({
    changeMenuItem: (payload) => 
        dispatch(actions.changeMenuItem(payload))
})

export default connect(mapStatetToProps, mapDispatchToProps)(Menu)