import React from 'react'
import * as colors from '../../../constants/Colors'
import LinearGradient from 'react-native-linear-gradient'
import MenuCell from '../../molecules/MenuCell'
import * as images from '../../../constants/Images'
import {px2dp} from '../../../utils/utils'
import { 
    View,
    FlatList,
    StyleSheet
} from 'react-native'

class Menu extends React.Component {
    rightShadow() {
        return <View style = {styles.rightShadow}>
            <LinearGradient
                colors = {['rgba(0, 0, 0, 0)', 'rgba(0, 0, 0, 0.1)']}
                style = {{width: '100%', height: '100%'}}
                start = {{x: 0, y: 0}}
                end = {{x: 1, y: 0}}
            />
        </View>
    }

    lineView() {
        return <View style = {styles.lineView}>
        </View>
    }

    menuList() {
        return <FlatList
            data = {this.props.items}
            extraData = {this.props.selectedItemIndex}
            renderItem = {({item, index}) => {
                return <MenuCell
                    selected = {index === this.props.selectedItemIndex}
                    title = {item.title}
                    selectedImage = {item.selectedImage}
                    image = {item.image}
                    style = {{marginLeft: px2dp(15)}}
                    onPress = {() => {
                        this.props.changeMenuItem({selectedItemIndex: index})
                    }}
                />
            }}
        />
    }

    render() {
        return (
            <View style = {styles.menu}>
                {this.rightShadow()}
                {this.lineView()}
                {this.menuList()}
            </View>
        )
    }
}

const styles = StyleSheet.create ({
    menu: {
        flex: 1,
        backgroundColor: colors.SNOW
    },

    rightShadow: {
        position: 'absolute',
        height: '100%',
        width: px2dp(13),
        alignSelf: 'flex-end',
        zIndex: 100
    },

    lineView: {
        height: px2dp(3),
        backgroundColor: colors.WHITE_SMOKE_LINE,
        marginTop: px2dp(126),
        marginLeft: px2dp(40)
    }
});

export default Menu