import React from 'react'
import LoginTextInput from '../atoms/LoginTextInput'
import ErrorTextWithImage from '../atoms/ErrorTextWithImage'
import LoginButtons from '../molecules/LoginButtons'
import { validateEmail, px2dp } from '../../utils/utils'
import * as colors from '../../constants/Colors'
import { 
    View,
    Text,
    StyleSheet
} from 'react-native'

class LoginContent extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            loginErrorText: '',
            passwordErrorText: ''
        }

        login = ''
        password = ''
    }

    validateLogin = () => {
        let errorText = validateEmail(this.login) ? '' : 'Email address is not in the correct format'
        this.setState({
            loginErrorText: errorText
        })

        return errorText === ''
    }

    validatePassword = () => {
        let errorText = this.password ? '' : 'Please enter your password'
        this.setState({
            passwordErrorText: errorText
        })

        return errorText === ''
    }

    loginScreenTitle() {
        return <Text style = {styles.title}>
            Sign in to access your account
        </Text>
    }

    loginErrorMessage() {
        if (this.props.isLoginSuccess) {
            return null
        }
        
        return <ErrorTextWithImage
            errorText = "Invalid email or password"
            style = {styles.loginErrorMessage}
        />
    }

    loginTextInput() {
        return  <LoginTextInput 
            style = {styles.inputText}
            placeholder = "enter email address"
            keyboardType = "email-address"
            inputHeader = "EMAIL"
            errorText = {this.state.loginErrorText} 
            onEndEditing = {this.validateLogin}
            onChangeText = {(text) => {
                this.login = text
            }}
        />
    }

    passwordTextInput() {
        return <LoginTextInput
            style = {styles.inputText}
            placeholder = "enter password"
            inputHeader = "PASSWORD"
            errorText = {this.state.passwordErrorText}
            onEndEditing = {this.validatePassword}
            secureTextEntry = {true}
            onEndEditing = {this.validatePassword}
            onChangeText = {(text) => {
                this.password = text
            }}
        />
    }

    loginButtons() {
        return <LoginButtons 
            style = {styles.loginButtons}
            signInButtonPressed = {() => {
                if (this.validateLogin(this.login) && this.validatePassword(this.password)) {
                    this.props.signInButtonPressed(this.login, this.password)
                }                
             }}
        />
    }

    render() {
        return (
            <View style = {this.props.style}>
                {this.loginScreenTitle()}
                {this.loginErrorMessage()}
                {this.loginTextInput()}
                {this.passwordTextInput()}
                {this.loginButtons()}
            </View>
        )
    }
}

const styles = StyleSheet.create ({
    title: {
        color : colors.CATALINA_BLUE,
        fontSize: px2dp(52)
    },

    loginErrorMessage: {
        width: px2dp(555),
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: px2dp(45)
    },

    inputText: {
        width: px2dp(555),
        marginTop: px2dp(45), 
        justifyContent: "center", 
        flexDirection: "column", 
        alignContent: "center"
    },

    loginButtons: {
        marginTop: px2dp(60)
    }
});

export default LoginContent