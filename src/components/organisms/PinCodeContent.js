import React from 'react'
import ErrorTextWithImage from '../atoms/ErrorTextWithImage'
import CalcFlatList from '../molecules/CalcFlatList'
import PinCodeSymbols from '../molecules/PinCodeSymbols'
import { px2dp } from '../../utils/utils'
import * as colors from '../../constants/Colors'
import * as images from '../../constants/Images'
import { 
    View,
    Text,
    StyleSheet
} from 'react-native'

const rowWidth = px2dp(800)
const back = 10
const zero = 11
const deleteSymbol = 12

class PinCodeContent extends React.Component {
    calcData = [{key: 1, title: '1'}, 
                {key: 2, title: '2'}, 
                {key: 3, title: '3'}, 
                {key: 4, title: '4'}, 
                {key: 5, title: '5'}, 
                {key: 6, title: '6'}, 
                {key: 7, title: '7'}, 
                {key: 8, title: '8'}, 
                {key: 9, title: '9'},
                {key: 10, title: 'logout', textColor: colors.CURIOUS_BLUE, fontWeight: 'normal', fontSize: px2dp(37)},
                {key: 11, title: '0'},
                {key: 12, image: images.arrow}]

    constructor(props) {
        super(props)
            
        this.state = {
            pin: '',
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.clearPincode) {
            this.setState({
                pin: ''
            })
        }
    }

    handlePress = (itemKey) => {
        pin = this.state.pin

        switch (itemKey) {
            case back:
                this.props.logout()
                break;
            case zero:
                pin += '0'
                break;
            case deleteSymbol: 
                pin = pin.substring(0, pin.length - 1)
                break;
            default:
                pin += `${itemKey}`
                break;
        }

        this.setState({
            pin: pin
        })

        if (pin.length == 4) {
            this.props.checkPincode(pin)
        }
    }

    loginScreenTitle() {
        return <Text style = {styles.title}>
            Enter pincode
        </Text>
    }

    errorPincode() {
        if (this.props.isPincodeCorrect == null || this.props.isPincodeCorrect) {
            return null            
        }
        
        return <ErrorTextWithImage
            errorText = "Incorrect pincode"
            style = {styles.loginErrorMessage}
        />
    }

    calcFlatList() {
        return <CalcFlatList 
            columnsNumber = {3}
            rowWidth = {rowWidth}
            data = {this.calcData}
            onPress = {this.handlePress}
        />
    }

    pinCodeView() {
        return <View style = {styles.pinCodeView}>
            <PinCodeSymbols
                pin = {this.state.pin}
            />
        </View>
    }

    calcView() {
        return <View style = {styles.calcView}>
            {this.calcFlatList()}
            {this.pinCodeView()}
        </View>
    }

    render() {
        return (
            <View style = {[this.props.style, styles.mainView]}>
                {this.loginScreenTitle()}
                {this.errorPincode()}
                {this.calcView()}
            </View>
        )
    }
}

const styles = StyleSheet.create ({
    mainView: {
        marginTop: px2dp(190)
    },

    title: {
        color : colors.CATALINA_BLUE,
        fontSize: px2dp(52)
    },

    pinCodeView: {
        height: px2dp(245),
        justifyContent: 'center',
        alignItems: 'stretch',
    },

    loginErrorMessage: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: px2dp(45)
    },

    calcView: {
        width: rowWidth,
        height: px2dp(790),
        borderRadius: 20,
        borderColor: colors.ALICE_BLUE,
        borderWidth: 2,
        overflow: 'hidden',
        flexDirection: 'column-reverse',
        marginTop: px2dp(50)
    }
});

export default PinCodeContent