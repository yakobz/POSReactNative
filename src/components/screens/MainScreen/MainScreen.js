import React from 'react'
import Spinner from 'react-native-loading-spinner-overlay'
import Drawer from 'react-native-drawer'
import Menu from '../../organisms/Menu/MenuContainer'
import Tables from '../../pages/Tables/Tables'
import CustomNavigationBar from '../../organisms/CustomNavigationBar/CustomNavigationBarContainer'
import { 
    View,
    StyleSheet,
} from 'react-native'

class MainScreen extends React.Component {
    mainScreen() {
        return <View style = {styles.mainScreen}>
            {this.navigationBar()}
            {this.tables()}
        </View>
    }

    tables() {
        return <Tables />
    }

    navigationBar() {
        return <CustomNavigationBar
            leftBarButtonPressed = {() => {
                this.drawer.open()
            }}
        />
    }

    render() {
        return (
            <Drawer
                ref = {(ref) => this.drawer = ref}
                content = {<Menu />}
                tapToClose
                openDrawerOffset = {0.73}
            >
                {this.mainScreen()}
            </Drawer>
        )
    }
}

const styles = StyleSheet.create ({
    mainScreen: {
        flex: 1,
        backgroundColor: 'white'
    }
});

export default MainScreen