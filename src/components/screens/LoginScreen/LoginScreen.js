import React from 'react'
import Spinner from 'react-native-loading-spinner-overlay'
import LoginContent from '../../organisms/LoginContent'
import * as screens from '../../../constants/Screens'
import * as images from '../../../constants/Images'
import { 
    View,
    StyleSheet,
    Image,
    Dimensions
} from 'react-native'

class LoginScreen extends React.Component {
    componentWillReceiveProps(nextProps) {
        if (nextProps.isLogin) {
            this.props.showPinCodeScreen(screens.PINCODE_SCREEN, false, null)
        }
    }

    viewWithImage() {
        return <View style = {styles.viewWithImage}>
            <Image
                style = {styles.bottomImage}
                source = {images.login}
            />
        </View>
    }

    render() {
        return (
            <View style = {styles.loginScreen}>
                <Spinner visible={this.props.loading} />
                <LoginContent 
                    style = {styles.contentView}
                    isLoginSuccess = {this.props.isLogin == null ? true : this.props.isLogin}
                    signInButtonPressed = {(login, password) => {
                        this.props.login({email: login, password: password})
                    }}
                />
                {this.viewWithImage()}
            </View>
        )
    }
}

const styles = StyleSheet.create ({
    loginScreen: {
        flex: 1,
        backgroundColor: 'white'
    },

    contentView: {
        flex: 0.66,
        alignItems: 'center',
        justifyContent: 'center'
    },

    viewWithImage: {
        flex: 0.34
    },

    bottomImage: {
        flex: 1,
        width: Dimensions.get("window").width
    }
});

export default LoginScreen