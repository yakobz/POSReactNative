import { connect } from 'react-redux'

import LoginScreen from './LoginScreen'
import loginActions from '../../../actions/login'
import screens from '../../../actions/screens'

const mapStatetToProps = store => ({
    isLogin: store.login.login,
    loading: store.login.loading
})

const mapDispatchToProps = dispatch => ({
    login: (payload) => 
        dispatch(loginActions.login(payload)),
    showPinCodeScreen: (route, reset, payload) =>
        dispatch(screens.showPinCodeScreen(route, reset, payload))
})

export default connect(mapStatetToProps, mapDispatchToProps)(LoginScreen)