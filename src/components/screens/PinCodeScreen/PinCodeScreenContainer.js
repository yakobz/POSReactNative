import PinCodeScreen from './PinCodeScreen'
import screens from '../../../actions/screens'
import loginActions from '../../../actions/login'
import { connect } from 'react-redux'

const mapStatetToProps = store => ({
    isPincodeCorrect: store.login.isPincodeCorrect,
    loading: store.login.loading
})

const mapDispatchToProps = dispatch => ({
    logout: (payload) => dispatch(screens.logout(payload)),
    checkPincode: (payload) => dispatch(loginActions.checkPincode(payload)),
    showMainScreen: (route, reset, payload) => dispatch(screens.showMainScreen(route, reset, payload))
})

export default connect(mapStatetToProps, mapDispatchToProps)(PinCodeScreen)