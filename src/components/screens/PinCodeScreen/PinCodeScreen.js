import React from 'react'
import Spinner from 'react-native-loading-spinner-overlay'
import PinCodeContent from '../../organisms/PinCodeContent'
import * as screens from '../../../constants/Screens'
import * as images from '../../../constants/Images'
import { 
    View,
    StyleSheet,
    ImageBackground,
    Dimensions
} from 'react-native'

class PinCodeScreen extends React.Component {
    clearPincode = false

    componentWillReceiveProps(nextProps) {
        this.clearPincode = (nextProps.loading === false && nextProps.isPincodeCorrect !== null)

        if (nextProps.loading === false && nextProps.isPincodeCorrect) {
            this.props.showMainScreen(screens.MAIN_SCREEN, false, null)
        }
    }

    backgroundImageWithContent() {
        return <ImageBackground
            style = {styles.backgroundImage}
            source = {images.pin_code_background}>
            <PinCodeContent 
                style = {styles.pinCodeContent} 
                isPincodeCorrect = {this.props.isPincodeCorrect}
                clearPincode = {this.clearPincode}
                logout = {() => {
                    this.props.logout({login: null, isPincodeCorrect: null})
                }}
                checkPincode = {(pincode) => {
                    this.props.checkPincode({pincode: pincode})
                }}
            />
        </ImageBackground>
    }

    render() {
        return (
            <View style = {styles.pinCodeView}>
                <Spinner visible={this.props.loading} />
                {this.backgroundImageWithContent()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    pinCodeView: {
        flex: 1,
        backgroundColor: 'white'
    },

    backgroundImage: {
        width: Dimensions.get("window").width,
        height: Dimensions.get("window").height
    },

    pinCodeContent: {
        flex: 1,
        alignItems: 'center'
    }
})

export default PinCodeScreen