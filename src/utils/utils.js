import { 
    Dimensions
} from 'react-native'

export function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email)
}

export function px2dp(size) {
    defaultHeight = 1536
    deviceHeight = Dimensions.get("window").height
    return size * deviceHeight / defaultHeight
}