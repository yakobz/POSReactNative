import * as types from '../constants/ActionTypes'

const initialState = {
    screen: null
}

export default function (state = initialState, action) {
    switch (action.type) {
        case types.SHOW_PIN_CODE_SCREEN:
            return state
        case types.SHOW_MAIN_SCREEN:
            return state
        default:
            return state
    }
}