import login from './login'
import screens from './screens'
import menu from './menu'
import { combineReducers } from 'redux'
import * as types from '../constants/ActionTypes'

const appReducer = combineReducers({
    login: login,
    screens: screens,
    menu: menu
})

const rootReducer = (state, action) => {
    if (action.type === types.LOGOUT) {
        state = undefined
    }

    return appReducer(state, action)
}

export default rootReducer