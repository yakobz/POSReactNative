import * as types from '../constants/ActionTypes'

const initialState = {
    login: null,
    isPincodeCorrect: null,
    loading: false
}

export default function (state = initialState, action) {
    switch (action.type) {
        case types.LOGIN_ACTION:
            return {...state, ...action.payload}
        case types.CHECK_PINCODE:
            return {...state, ...action.payload}
        default:
            return state
    }
}