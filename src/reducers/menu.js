import * as types from '../constants/ActionTypes'
import * as images from '../constants/Images'
import Strings from '../constants/Strings'

const initialState = {
    selectedItemIndex: 0,
    items: [
        {key: 0, title: Strings.main_screen, image: images.home, selectedImage: images.homeactive, containItems: false},
        {key: 1, title: Strings.order_archive, image: images.archive, selectedImage: images.archiveactive, containItems: true},
        {key: 2, title: Strings.cash_in_out, image: images.cash, selectedImage: images.cashactive, containItems: false},
        {key: 3, title: Strings.devices_settigs, image: images.device, selectedImage: images.deviceactive, containItems: true},
        {key: 4, title: Strings.worklog_staff, image: images.staff, selectedImage: images.staffactive, containItems: false},
        {key: 5, title: Strings.reports, image: images.reports, selectedImage: images.reportsactive, containItems: true},
        {key: 6, title: Strings.checked_in_clients, image: images.checkin, selectedImage: images.checkinactive, containItems: false},
        {key: 7, title: Strings.info, image: images.info, selectedImage: images.infoactive, containItems: false}
    ]
}

export default function (state = initialState, action) {
    switch (action.type) {
        case types.CHANGE_MENU_ITEM:
            return {...state, ...action.payload}
        default:
            return state
    }
}