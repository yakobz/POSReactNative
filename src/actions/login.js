import * as types from '../constants/ActionTypes'
import api from '../api'
import store from '../store'

const login = (payload) => {
    return {
        type: types.API,
        action: types.LOGIN_ACTION,
        promise: api.login(payload.email, payload.password)
    }
}

const checkPincode = (payload) => {
    return {
        type: types.API,
        action: types.CHECK_PINCODE,
        promise: api.checkPincode(payload.pincode)
    }
}

export default {
    login,
    checkPincode
}