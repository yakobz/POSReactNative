import * as types from '../constants/ActionTypes'

const changeMenuItem = (payload) => {
    return {
        type: types.CHANGE_MENU_ITEM,
        payload: payload
    }
}

export default {
    changeMenuItem
}