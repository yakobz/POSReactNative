import {Actions, ActionConst} from 'react-native-router-flux'
import * as types from '../constants/ActionTypes'
import * as screens from '../constants/Screens'

const showPinCodeScreen = (route, reset, payload) => {
    if (reset) {
        Actions[route]({type: ActionConst.RESET})
    }

    Actions[route]()

    return {
        type: types.SHOW_PIN_CODE_SCREEN,
        reset: reset,
        payload: payload
    }
}

const logout = (payload) => {
    Actions.popTo(screens.LOGIN_SCREEN)

    return {
        type: types.LOGOUT,
        payload: payload
    }
}

const showMainScreen = (route, reset, payload) => {
    if (reset) {
        Actions[route]({type: ActionConst.RESET})
    }

    Actions[route]()

    return {
        type: types.SHOW_MAIN_SCREEN,
        reset: reset,
        payload: payload
    }
}

export default {
    showPinCodeScreen,
    logout,
    showMainScreen
}