import React, {AsyncStorage} from 'react-native'
import reducers from './reducers'
import { createStore, applyMiddleware, compose} from 'redux'
import { createLogger } from 'redux-logger'
import { persistStore, autoRehydrate } from 'redux-persist'
import thunkMiddleware from 'redux-thunk'
import apiMiddleware from './middlewares/api'

const loggerMiddleware = createLogger({ predicate: (getState, action) => __DEV__ })

const middlewares = compose(
    applyMiddleware(
        thunkMiddleware,
        loggerMiddleware,
        apiMiddleware
    )
)

// const store = createStore(reducers, {}, compose(middlewares, autoRehydrate()))
const store = createStore(reducers, {}, middlewares)

export default store